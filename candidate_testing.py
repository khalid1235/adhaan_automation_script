import pdb
import requests
from selenium import webdriver
import os
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://adhaancandidate.azurewebsites.net/blue/login')

mobileNo = "8899747404"

driver.find_element_by_class_name("form-control").send_keys(mobileNo)
mobileSubmitBtn = driver.find_element_by_class_name("account-btn")
mobileSubmitBtn.click()
# if driver.find_element_by_class_name("text-danger"):
#     print("error")
# else:
#     print("redirected to OTP verification page")
time.sleep(0.5)
url = "https://aadhaan.ddns.net/api/users/get-otp/{}".format(mobileNo)

response = requests.get(url)
otp = str(response.json().get("otp", None))

driver.find_element_by_xpath("//input[1]").send_keys(otp[0])
driver.find_element_by_xpath("//input[2]").send_keys(otp[1])
driver.find_element_by_xpath("//input[3]").send_keys(otp[2])
driver.find_element_by_xpath("//input[4]").send_keys(otp[3])

otpSubmitBtn = driver.find_element_by_xpath("//button[@type='submit']")
otpSubmitBtn.click()
time.sleep(2)
# for i in range(4):
#     print("sleeping for {} sec".format(i))
#     time.sleep(1)

driver.find_element_by_xpath('//button[1]').click()
time.sleep(2)
driver.find_element_by_xpath("//button[contains(text(),'Yes')]").click()


driver.find_element_by_xpath(
    "//body/div[@id='app']/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/a[1]/i[1]").click()
